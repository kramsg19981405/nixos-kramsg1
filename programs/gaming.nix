nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [
    "steam"
    "steam-original"
    "steam-runtime"
    "steam-run"
    "intel-ocl"
    "google-chrome"
    "davinci-resolve"
    "nvidia-x11"
    "nvidia-settings"
    "geekbench"
  ];

  programs.steam.enable = true;
  hardware.steam-hardware.enable = true;

  }
