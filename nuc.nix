# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.useOSProber = true;
  boot.kernelPackages = pkgs.linuxPackages_5_17;

  networking.hostName = "Torrent-PC"; # Define your hostname.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Australia/Sydney";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_AU.utf8";

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the Cinnamon Desktop Environment.
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;
  ## bluetooth
  
# suport ntfs
  boot.supportedFilesystems = [ "ntfs" ];


  # Configure keymap in X11
  services.xserver = {
    layout = "au";
    xkbVariant = "";
  };


  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.kramsg1 = {
    isNormalUser = true;
    description = "Mark Harter";
    extraGroups = [ "networkmanager" "wheel" ];
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDKGeRPg/7tnegQWCQA2FRBHmhBUDPWoHid10/uOeVUcPdYOUv/JcqFqw/8Wkeffl0Bm94gSxiY17jy6EcQK/7M9LuQxev0/RTqCl9KSQI3qhkIVUxwy6t5B3qwjF88GAjxJKTiujRI135/kFBJkgSAW5CSJ7HU1BO0x+LTHjU04grMt7XegtUu83ypV9gByn9widnEfz2ei/OdxLkYygGfiIrCWER1omlM4CJ/UWRtiFv6cFca1iHdwyhrnLfZ9kg7o4ibl0z6D8ejAaUT3p6EhuiFoBkeUDHcy6dZXI/VlJqbbUqBbKrNJciSx8JWIAkDKxanL8C2YZNc5AmF5SSPhOHv/Rn88XR/vuHj1rMI3Z2ZIqt61ZueW4tOR+A+3k+YV20lA+utqPwa5XLaDu0bfj2PD1MizqlNICRxPRnqC18Pw2l4Ir1Bqr+FpTCX1I5KT71VrnyGBkaaHaduIjurLrToLl8VPk5hlF+oUdUlU067trR/Ax7/S3L1UXW0YI7V+PWKNuhe3zI2/y/qT34wDsiO+0dHP/E1Bn/6wguONca6Cv108c+CIDrUdSV+V8De4ip0MeSetiM7+jBYePGTfwn5snYeaHoeDKfvH45tMSRJcg278dvc7+i/mswsIBs4vmBPGyPYuNrvpgzcn/OmdQ6msgJhnPz4rQSb7MTv2w== cardno:9
" ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
  #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    brave
    bitwarden
    plex
    git
    qbittorrent
    openvpn

    
    

  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
   programs.gnupg.agent = {
     enable = true;
     enableSSHSupport = true;
     pinentryFlavor = "gtk2";
   };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
   networking.firewall.allowedTCPPorts = [8080];
  networking.firewall.allowedUDPPorts = [8080];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leavecatenate(variables, "bootdev", bootdev)
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  
fileSystems."/hdd/hdd" =
  { device = "/dev/disk/by-uuid/5049f41a-6700-4da9-9907-e4a8119c6ed9";
    fsType = "ext4";
  };  

## plex
  services.plex = {
  enable = true;
  openFirewall = true;
};

  
  system.stateVersion = "22.05"; # Did you read the comment?


}
