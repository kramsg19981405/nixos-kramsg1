# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      ./yubikey-gpg.nix
      ./keybase.nix
      ./cachix.nix



    ];
nix.settings = {
    substituters = ["https://nix-gaming.cachix.org"];
    trusted-public-keys = ["nix-gaming.cachix.org-1:nbjlureqMbRAxR1gJ/f3hxemL9svXaZF/Ees8vCUUs4="];
  };
  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;


  networking.hostName = "Mark-PC"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Australia/Sydney";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_AU.utf8";

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the Cinnamon Desktop Environment.
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;
  ## bluetooth
  services.blueman.enable = true;
  hardware.bluetooth.enable = true;

# suport ntfs
  boot.supportedFilesystems = [ "ntfs" ];

# virtmanager
virtualisation.libvirtd.enable = true;

services.xserver.displayManager.setupCommands = ''
    LEFT='HDMI-1'
    CENTER='HDMI-0'
    ${pkgs.xorg.xrandr}/bin/xrandr --output $CENTER --rotate left --output $LEFT
    '';

  # Configure keymap in X11
  services.xserver = {
    layout = "au";
    xkbVariant = "";
  };


  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.kramsg1 = {
    isNormalUser = true;
    description = "Mark Harter";
    extraGroups = [ "networkmanager" "wheel" "qemu-libvirtd" "libvirtd" ];
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDKGeRPg/7tnegQWCQA2FRBHmhBUDPWoHid10/uOeVUcPdYOUv/JcqFqw/8Wkeffl0Bm94gSxiY17jy6EcQK/7M9LuQxev0/RTqCl9KSQI3qhkIVUxwy6t5B3qwjF88GAjxJKTiujRI135/kFBJkgSAW5CSJ7HU1BO0x+LTHjU04grMt7XegtUu83ypV9gByn9widnEfz2ei/OdxLkYygGfiIrCWER1omlM4CJ/UWRtiFv6cFca1iHdwyhrnLfZ9kg7o4ibl0z6D8ejAaUT3p6EhuiFoBkeUDHcy6dZXI/VlJqbbUqBbKrNJciSx8JWIAkDKxanL8C2YZNc5AmF5SSPhOHv/Rn88XR/vuHj1rMI3Z2ZIqt61ZueW4tOR+A+3k+YV20lA+utqPwa5XLaDu0bfj2PD1MizqlNICRxPRnqC18Pw2l4Ir1Bqr+FpTCX1I5KT71VrnyGBkaaHaduIjurLrToLl8VPk5hlF+oUdUlU067trR/Ax7/S3L1UXW0YI7V+PWKNuhe3zI2/y/qT34wDsiO+0dHP/E1Bn/6wguONca6Cv108c+CIDrUdSV+V8De4ip0MeSetiM7+jBYePGTfwn5snYeaHoeDKfvH45tMSRJcg278dvc7+i/mswsIBs4vmBPGyPYuNrvpgzcn/OmdQ6msgJhnPz4rQSb7MTv2w== cardno:9
"
"ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCkJsAE9vFFNQIi77T1QTH4VAh25q8hoJfqefxSvLsrfwUUd7hINIWPvRTQXyBV25gVoZ7gcgBtxD09h12yf8bu0yVH4CVkZHwQXrUPTRsbx8ycLCsla05CH4VcdXmw2qX+fwUNE2OqliLLLe1v6aLNomWjHpEHik3d9FOqdB+/84ZD4SIA3cFRakTc0KqCPA3dfJi9yTEoB+tggBIsEAnETju3TqcajtmeUqrC+sCEA3MT9iK7IiW5WIbWMpb1LoZ7uqJrMN51JNVZ9g5FW2MXdPfu/41cylzKDLeD0XYeFTyFzgHmMlW+GV62Fq5zsKRoTBn0rYtDgt9j/aOMqPCBoRcr9QgqGQxP11q2xArFWWL8UHKBbrJeG2IgYTbkmppy/zPMkB9PTIoWZaZTTNRFZ7zsSepz/frYxS+sdCpXhU7ojpmfHCoVCM9FfEvpGwrbq9rcCQHnIA1bB42+Hy6LuBlMNbbdWFyyjciK0it3h6UO+FLjvY0fX4oXGrRQ/tFJXTIBh4hJh8gB0HsnpmQOaKplz7z7Wa/xeikvgcgKnMjBAf1ZJM31au3LmcL5Mn+G3Eu0ZXwTPgFp2rMLBQfkOpv6UKmikJS8cODEWCpwNnmxlGtL2p8yM53o8plH2RkHJsiWqbG/4QgFYxrfSRbEi0ev1J27hYvfjEpt97yTTQ== cardno:13_792_382
"
];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;
# nvidia
 hardware.nvidia.nvidiaSettings = true;
  services.xserver.videoDrivers = ["nvidia"];
  hardware.opengl.enable = true;
  hardware.nvidia.modesetting.enable = true;
  hardware.opengl.driSupport32Bit = true;
  hardware.nvidia.powerManagement.enable = true;
    hardware.nvidia.package = config.boot.kernelPackages.nvidiaPackages.stable;


  # Flatpacks
    services.flatpak.enable = true;
    xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
  #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    brave
    kate
    signal-desktop
    zoom-us
    lutris
    steam
    wine-staging
    bitwarden
    plex-media-player
    kleopatra
    git
    discord
    vmware-workstation
    filezilla
    openssl
    obsidian
    variety
    keybase
    keybase-gui
    kbfs
    dxvk
    protontricks
    protonup-qt
    vkd3d-proton
    mesa
    vulkan-validation-layers
    xorg.xf86videoamdgpu
    mangohud
    wineWowPackages.staging
    winetricks
    flatpak-builder
    flatpak
    gnome.gnome-software
    linuxKernel.packages.linux_6_4.nvidia_x11_vulkan_beta
    nvidia-vaapi-driver
    vagrant
    qemu
    virt-manager


  ];

  programs.steam.enable = true;
  hardware.steam-hardware.enable = true;



  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
   programs.gnupg.agent = {
     enable = true;
     enableSSHSupport = true;
     pinentryFlavor = "gtk2";
   };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leavecatenate(variables, "bootdev", bootdev)
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).


  fileSystems."/hdd/4tb" =
  { device = "/dev/sda1";
    fsType = "ext4";
  };

  system.stateVersion = "23.05"; # Did you read the comment?


}
